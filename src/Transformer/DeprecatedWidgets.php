<?php

namespace Ucdavis\SitefarmTransformers\Transformer;

use Ucdavis\SitefarmTransformers\HtmlTransformBase;
use IvoPetkov\HTML5DOMElement;

/**
 * Class DeprecatedWidgets.
 *
 * Convert deprecated widget markup into valid markup.
 */
class DeprecatedWidgets extends HtmlTransformBase {

  /**
   * Transform the html code into the new format.
   *
   * @return string
   *   The Html code after finishing transformations.
   */
  public function process(): string {
    $this->fullBleedWrapper();
    $this->doublePhotoWrapper();
    $this->largeImageWrapper();
    $this->personFeature();
    return $this->html;
  }

  /**
   * Convert Media Link Markup into web component.
   */
  public function fullBleedWrapper(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('.full-bleed__wrapper') as $element) {
      $content = $element->querySelector('.full-bleed__figure');
      $image = $this->getImageMarkup($content);

      if ($image) {
        $markup = <<<EOT
<layout-columns columns="1" cwidth="full">
  <div slot="column1">${image}</div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT;

        $this->replaceElement($element, $markup);
      }
    }

    $this->saveDomToHtml();
  }

  /**
   * Convert Double Photo Wrapper Markup into web component.
   */
  public function doublePhotoWrapper(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('.double-photo__wrapper') as $element) {
      $left_image = $element->querySelector('.double-photo__left');
      $left_image = $this->getImageMarkup($left_image);

      $right_image = $element->querySelector('.double-photo__right');
      $right_image = $this->getImageMarkup($right_image);

      $markup = <<<EOT
<layout-columns columns="2">
  <div slot="column1">${left_image}</div>
  <div slot="column2">${right_image}</div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

  /**
   * Convert Large Image Wrapper Markup into web component.
   */
  public function largeImageWrapper(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('.large-image__wrapper') as $element) {
      $content = $element->querySelector('.large-image__figure');
      $image = $this->getImageMarkup($content);

      $markup = <<<EOT
<layout-columns columns="1" cwidth="overflow">
  <div slot="column1">${image}</div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

  /**
   * Convert Feature Person markup into a Feature Block.
   */
  public function personFeature(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('.person-feature') as $element) {
      $figure = trim($element->querySelector('.person-feature__image')->innerHTML);
      $name = trim($element->querySelector('.person-feature__name')->innerHTML);
      $title = $element->querySelector('.person-feature__title');
      $body = trim($element->querySelector('.person-feature__body')->innerHTML);
      $link = $element->querySelector('.person-feature__link a');

      if ($title) {
        // Remove the title if it is supposed to be hidden.
        if ($title->classList->contains('is-hidden')) {
          $title = '';
        }
        else {
          $title = '<h4>' . trim($title->innerHTML) . '</h4>';
        }
      }

      if ($link) {
        $link->classList->add('btn--primary');
        $link = '<p>' . $link->outerHTML . '</p>';
      }

      // Add the title and link to the body.
      $body = trim($title . $body . $link);

      $class = FALSE;
      if ($element->classList->contains('align-right') || $element->classList->contains('u-align--right')) {
        $class = 'u-width--half u-align--right';
      }
      elseif ($element->classList->contains('align-left') || $element->classList->contains('u-align--left')) {
        $class = 'u-width--half u-align--left';
      }

      $attr = $this->createHtmlAttr([
        'figure' => ($figure) ? 'true' : FALSE,
        'class' => $class,
      ]);

      $markup = <<<EOT
<feature-block${attr}>
	<div slot="figure">${figure}</div>
	<div slot="title">${name}</div>
	<div slot="body">$body</div>
</feature-block>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

  /**
   * Get the image markup that is inside and DomElement.
   *
   * @param \IvoPetkov\HTML5DOMElement $element
   *   The DOM element to find and <img> or <drupal-media> in
   *
   * @return string
   *   Image markup with a caption attached as a data attribute.
   */
  protected function getImageMarkup(HTML5DOMElement $element) {
    $image = $element->querySelector('img');
    // If this uses Media then use that instead of an Image.
    if (!$image) {
      $image = $element->querySelector('drupal-media');
    }
    // Likely an empty element.
    if (!$image){
      return FALSE;
    }
    // Remove the image and then the remaining html should be the caption.
    $element->removeChild($image);
    $caption = trim($element->innerHTML);
    if ($caption) {
      $image->setAttribute('data-caption', $caption);
    }
    return $image->outerHTML;
  }

}
