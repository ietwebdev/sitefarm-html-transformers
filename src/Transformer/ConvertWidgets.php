<?php

namespace Ucdavis\SitefarmTransformers\Transformer;

use Ucdavis\SitefarmTransformers\HtmlTransformBase;

/**
 * Class ConvertWidgets.
 *
 * Convert old widget markup into new Web Component style markup.
 */
class ConvertWidgets extends HtmlTransformBase {

  /**
   * Transform the html code into the new format.
   *
   * @return string
   *   The Html code after finishing transformations.
   */
  public function process(): string {
    $this->mediaLink();
    $this->featureBlock();
    return $this->html;
  }

  /**
   * Convert Media Link Markup into web component.
   */
  public function mediaLink(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('.media-link__wrapper') as $element) {
      $figure = trim($element->querySelector('.media-link__figure')->innerHTML);
      $title = trim($element->querySelector('.media-link__title')->innerHTML);
      $content = trim($element->querySelector('.media-link__content')->innerHTML);

      $attr = $this->createHtmlAttr([
        'url' => $element->getAttribute('data-url'),
      ]);

      $markup = <<<EOT
<media-link${attr}>
  <div slot="image">${figure}</div>
  <div slot="title">${title}</div>
  <div slot="content">${content}</div>
</media-link>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

  /**
   * Convert Feature Block Markup into web component.
   */
  public function featureBlock(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('.wysiwyg-feature-block') as $element) {
      // Fetch the element again fresh to allow nesting.
      $element = $this->dom->querySelector('.wysiwyg-feature-block');

      $figure = '';
      $figure_element = $element->querySelector('.wysiwyg-feature-block__figure');
      if ($figure_element) {
        $figure = $figure_element->innerHTML;
      }
      $title = trim($element->querySelector('.wysiwyg-feature-block__title')->innerHTML);
      $content = trim($element->querySelector('.wysiwyg-feature-block__body')->innerHTML);

      $element->classList->remove('wysiwyg-feature-block');

      $attr = $this->createHtmlAttr([
        'class' => $element->classList->value,
        'figure' => ($figure) ? 'true' : FALSE,
      ]);

      $markup = <<<EOT
<feature-block${attr}>
  <div slot="figure">${figure}</div>
  <div slot="title">${title}</div>
  <div slot="body">${content}</div>
</feature-block>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

}
