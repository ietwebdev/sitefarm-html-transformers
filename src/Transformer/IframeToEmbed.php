<?php

namespace Ucdavis\SitefarmTransformers\Transformer;

use Ucdavis\SitefarmTransformers\HtmlTransformBase;

/**
 * Class IframeToEmbed.
 *
 * Convert iFrame markup into Url Embeds.
 */
class IframeToEmbed extends HtmlTransformBase {

  /**
   * Transform the html code into the new format.
   *
   * @return string
   *   The Html code after finishing transformations.
   */
  public function process(): string {
    $this->customSoundcloud();
    // Do iframely last so it doesn't think custom soundclouds belong to it.
    $this->iframely();
    return $this->html;
  }

  /**
   * Convert iFramely markup into a <drupal-url> tag.
   */
  public function iframely(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('[data-oembed-url]') as $element) {
      $url = $element->getAttribute('data-oembed-url');

      $provider = '';

      if (preg_match('/(youtube\.com|youtu\.be)/', $url, $matches)) {
        $provider = 'YouTube';
      }
      if (strpos($url, 'vimeo.com') !== FALSE) {
        $provider = 'Vimeo';
      }
      if (strpos($url, 'soundcloud.com') !== FALSE) {
        $provider = 'SoundCloud';
      }

      $markup = <<<EOT
<drupal-url data-embed-button="url" data-embed-url="${url}" data-entity-label="URL" data-url-provider="${provider}"></drupal-url>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

  /**
   * Convert custom Soundcloud iframes into a <drupal-url> tag.
   */
  public function customSoundcloud(): void {
    /** @var \IvoPetkov\HTML5DOMElement $element */
    foreach ($this->dom->querySelectorAll('[data-oembed-url*="podbean"]') as $element) {
      $iframe_src = $element->querySelector('iframe')->getAttribute('src');

      preg_match('/.+tracks\/\d+/', $iframe_src, $matches);
      if (!$matches) {
        return;
      }

      $url = $matches[0];

      $markup = <<<EOT
<drupal-url data-embed-button="url" data-embed-url="${url}" data-entity-label="URL" data-url-provider="SoundCloud"></drupal-url>
EOT;

      $this->replaceElement($element, $markup);
    }

    $this->saveDomToHtml();
  }

}
