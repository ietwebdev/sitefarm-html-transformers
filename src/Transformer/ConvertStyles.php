<?php

namespace Ucdavis\SitefarmTransformers\Transformer;

use Ucdavis\SitefarmTransformers\HtmlTransformBase;

/**
 * Class ConvertStyles.
 *
 * Convert old styles to new css classes and markup.
 */
class ConvertStyles extends HtmlTransformBase {

  /**
   * Old to New classes.
   *
   * @var array
   */
  protected $replaceClassMap = [
    'rtecenter' => 'text-align-center',
    'rteright' => 'text-align-right',
    'pullquote--right' => 'pullquote u-align--right u-width--half',
    'pullquote--left' => 'pullquote u-align--left u-width--half',
    'category-brand__text heading--centerline' => 'heading--underline text-align-center',
    'heading--centerline category-brand__text' => 'heading--underline text-align-center',
    'btn--pill category-brand__btn--pill' => 'btn--primary btn--round',
    'l-column--2 list--link' => 'l-column--2',
    'l-column--3 list--link' => 'l-column--3',
    'list--divided' => 'list--arrow',
    'list--link' => 'list--arrow',
    // These likely aren't allowed in our ckeditor and are likely on images.
    // Images will turn into Media and instead use data-align="left" attributes.
//    'align-left width-half' => 'u-align--left u-width--half',
//    'align-right width-half' => 'u-align--right u-width--half',
//    'align-center width-two-thirds' => 'l-shrink',
  ];

  /**
   * Inline styles to css class mapping.
   *
   * @var array
   */
  protected $inlineMap = [
    'margin-left:40px' => 'indent',
    'margin-left:80px' => 'indent--large',
    'margin-left:120px' => 'indent--giant',
  ];

  /**
   * Classes to be removed.
   *
   * @var array
   */
  protected $removeClassMap = [
    'image',
  ];

  /**
   * Set the replaceClass property.
   *
   * @param array $replaceClassMap
   */
  public function setReplaceClassMap(array $replaceClassMap): void {
    $this->replaceClassMap = $replaceClassMap;
  }

  /**
   * Set the inlineMap property.
   *
   * @param array $inlineMap
   */
  public function setInlineMap(array $inlineMap): void {
    $this->inlineMap = $inlineMap;
  }

  /**
   * Set the removeClassMap property.
   *
   * @param array $removeClassMap
   */
  public function setRemoveClassMap(array $removeClassMap): void {
    $this->removeClassMap = $removeClassMap;
  }

  /**
   * Transform the html code into the new format.
   *
   * @return string
   *   The Html code after finishing transformations.
   */
  public function process(): string {
    $this->inlineStyles();
    $this->removeClasses();
    // Perform straight string replacements last since they do not use Dom.
    $this->replaceClasses();
    return $this->html;
  }

  /**
   * Replace old classes with new ones.
   */
  public function replaceClasses(): void {
    $search = array_keys($this->replaceClassMap);
    $replace = array_values($this->replaceClassMap);
    $this->html = str_replace($search, $replace, $this->html);
  }

  /**
   * Remove deprecated classes that are no longer used.
   */
  public function removeClasses(): void {
    foreach ($this->removeClassMap as $search) {
      /** @var \IvoPetkov\HTML5DOMElement $element */
      foreach ($this->dom->querySelectorAll(".${search}") as $element) {
        $element->classList->remove($search);

        if ($element->classList->length == 0) {
          $element->removeAttribute('class');
        }

        $element->removeAttribute('style');
      }
    }

    $this->saveDomToHtml();
  }

  /**
   * Convert inline styles into classes on the same element.
   */
  public function inlineStyles(): void {
    foreach ($this->inlineMap as $search => $replace) {
      /** @var \IvoPetkov\HTML5DOMElement $element */
      foreach ($this->dom->querySelectorAll("[style=\"${search}\"]") as $element) {
        $element->classList->add($replace);
        $element->removeAttribute('style');
      }
    }

    $this->saveDomToHtml();
  }

}
