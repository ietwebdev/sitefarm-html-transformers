<?php

namespace Ucdavis\SitefarmTransformers;

use IvoPetkov\HTML5DOMDocument;
use IvoPetkov\HTML5DOMElement;

/**
 * Class HtmlTransformBase.
 *
 * Base class to convert old styles to new css classes and markup.
 */
abstract class HtmlTransformBase implements HtmlTransformInterface {

  /**
   * The html content needing parsed.
   *
   * @var string
   */
  protected $html;

  /**
   * The DomDocument of the html.
   *
   * @var \IvoPetkov\HTML5DOMDocument
   */
  protected $dom;

  /**
   * HtmlParser constructor.
   *
   * @param string $html
   *   The string of html text.
   */
  public function __construct(string $html) {
    $this->setHtml($html);
  }

  /**
   * Get the Html String.
   *
   * @return string
   */
  public function getHtml(): string {
    return $this->html;
  }

  /**
   * Set the HTML String.
   *
   * @param string $html
   */
  public function setHtml(string $html): void {
    $this->html = $html;
    $this->updateDom();
  }

  /**
   * Set the Dom Document based on the html property.
   */
  protected function updateDom(): void {
    $dom = new HTML5DOMDocument();
    @$dom->loadHTML($this->html, HTML5DOMDocument::ALLOW_DUPLICATE_IDS);
    $this->dom = $dom;
  }

  /**
   * Convert the Dom back into an html string.
   *
   * @return string
   */
  protected function saveDomToHtml(): string {
    $this->html = $this->dom->querySelector('body')->innerHTML;
    if(empty($this->html)){
      return "";
    }
    return $this->html;
  }

  /**
   * Replace a Dom Element.
   *
   * @param \IvoPetkov\HTML5DOMElement $element
   *   A DOMElement object.
   * @param string $content
   *   The text or HTML that will replace the element.
   */
  protected function replaceElement(HTML5DOMElement &$element, $content): void {
    if (strlen($content)) {
      $dom = new HTML5DOMDocument();
      @$dom->loadHTML($content);

      // Load the content into a new DOMDocument and retrieve the DOM nodes.
      $replacement_nodes = $dom->querySelector('body')->childNodes;
    }
    else {
      $replacement_nodes = [$element->ownerDocument->createTextNode('')];
    }

    foreach ($replacement_nodes as $replacement_node) {
      // Import the replacement node from the new DOMDocument into the original
      // one, importing also the child nodes of the replacement node.
      $replacement_node = $element->ownerDocument->importNode($replacement_node, TRUE);
      $element->parentNode->insertBefore($replacement_node, $element);
    }
    $element->parentNode->removeChild($element);
  }

  /**
   * Generate a string of html attributes.
   *
   * @param array $attributes
   *   The names and values of each attribute.
   *
   * @return string
   *   All of the attributes in a string of html.
   */
  protected function createHtmlAttr(array $attributes): string {
    $html = '';
    foreach ($attributes as $name => $value) {
      if ($value === TRUE) {
        $html .= " ${name}";
      }
      elseif ($value) {
        $html .= " ${name}=\"${value}\"";
      }
    }
    return $html;
  }

}
