<?php

namespace Ucdavis\SitefarmTransformers;

/**
 * Interface HtmlTransformInterface.
 *
 * Defines an interface for HTML Transformers.
 *
 * @package Ucdavis\SitefarmTransformers
 */
interface HtmlTransformInterface {

  /**
   * Transform the html code into the new format.
   *
   * @return string
   *   The Html code after finishing transformations.
   */
  public function process();

}
