<?php

namespace Ucdavis\SitefarmTransformers;

use Ucdavis\SitefarmTransformers\Transformer\ConvertStyles;
use Ucdavis\SitefarmTransformers\Transformer\ConvertWidgets;
use Ucdavis\SitefarmTransformers\Transformer\DeprecatedWidgets;
use Ucdavis\SitefarmTransformers\Transformer\IframeToEmbed;

/**
 * Class TransformAll.
 *
 * This is purely a convenience class to run all of the transformers at onece.
 *
 * @package Ucdavis\SitefarmTransformers
 */
class TransformAll {

  /**
   * Process Markup with all the transformers.
   *
   * @param $html
   *   The markup needing transforming.
   *
   * @return string
   *   The processed html.
   */
  public static function process(string $html): string {
    // Convert the old classes to new ones.
    $convertStyles = new ConvertStyles($html);
    $html = $convertStyles->process();

    $convertWidgets = new ConvertWidgets($html);
    $html = $convertWidgets->process();

    $deprecatedWidgets = new DeprecatedWidgets($html);
    $html = $deprecatedWidgets->process();

    $iframeToEmbed = new IframeToEmbed($html);
    $html = $iframeToEmbed->process();

    return $html;
  }

}
