<?php

namespace Ucdavis\SitefarmTransformers\Tests;

use PHPUnit\Framework\TestCase;
use Ucdavis\SitefarmTransformers\Transformer\DeprecatedWidgets;

/**
 * Class DeprecatedWidgets.
 *
 * @coversDefaultClass \Ucdavis\SitefarmTransformers\Transformer\DeprecatedWidgets
 */
class DeprecatedWidgetsTest extends TestCase {

  /**
   * Tests the fullBleedWrapper method.
   *
   * @dataProvider fullBleedProvider
   *
   * @param $html
   *   The entered markup.
   * @param $expected
   *   The expected returned html after formatting.
   *
   * @see ::fullBleedWrapper()
   */
  public function testFullBleedWrapper($html, $expected): void {
    $service = new DeprecatedWidgets($html);
    $service->fullBleedWrapper();
    $this->assertEquals($expected, $service->getHtml());
  }

  /**
   * Provider for testFullBleedWrapper()
   */
  public function fullBleedProvider() {
    // Given markup, expected markup.
    return [
      [
        <<<EOT
<div class="full-bleed__wrapper">
  <div class="full-bleed__figure">
    <img alt="UC Davis" src="http://placehold.it/3000x1155">Caption
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="full"><div slot="column1"><img alt="UC Davis" src="http://placehold.it/3000x1155" data-caption="Caption"></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="full-bleed__wrapper">
  <div class="full-bleed__figure">
    <img alt="UC Davis" src="http://placehold.it/3000x1155">
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="full"><div slot="column1"><img alt="UC Davis" src="http://placehold.it/3000x1155"></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="full-bleed__wrapper">
  <div class="full-bleed__figure">
    <drupal-media data-align="center" data-entity-type="media" data-entity-uuid="123abc"></drupal-media>Caption
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="full"><div slot="column1"><drupal-media data-align="center" data-entity-type="media" data-entity-uuid="123abc" data-caption="Caption"></drupal-media></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="full-bleed__wrapper">
  <div class="full-bleed__figure">
    <drupal-media data-entity-type="media"></drupal-media>
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="full"><div slot="column1"><drupal-media data-entity-type="media"></drupal-media></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],
    ];
  }

  /**
   * Tests the largeImageWrapper method.
   *
   * @dataProvider largeImageProvider
   *
   * @param $html
   *   The entered markup.
   * @param $expected
   *   The expected returned html after formatting.
   *
   * @see ::largeImageWrapper()
   */
  public function testLargeImageWrapper($html, $expected): void {
    $service = new DeprecatedWidgets($html);
    $service->largeImageWrapper();
    $this->assertEquals($expected, $service->getHtml());
  }

  /**
   * Provider for testLargeImageWrapper()
   */
  public function largeImageProvider() {
    // given markup, expected markup.
    return [
      [
        <<<EOT
<div class="large-image__wrapper">
  <div class="large-image__figure">
    <img alt="UC Davis" src="http://placehold.it/3000x1155">Caption
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="overflow"><div slot="column1"><img alt="UC Davis" src="http://placehold.it/3000x1155" data-caption="Caption"></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="large-image__wrapper">
  <div class="large-image__figure">
    <img alt="UC Davis" src="http://placehold.it/3000x1155">
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="overflow"><div slot="column1"><img alt="UC Davis" src="http://placehold.it/3000x1155"></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="large-image__wrapper">
  <div class="large-image__figure">
    <drupal-media data-align="center" data-entity-type="media" data-entity-uuid="123abc"></drupal-media>Caption
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="overflow"><div slot="column1"><drupal-media data-align="center" data-entity-type="media" data-entity-uuid="123abc" data-caption="Caption"></drupal-media></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="large-image__wrapper">
  <div class="large-image__figure">
    <drupal-media data-entity-type="media"></drupal-media>
  </div>
</div>
EOT,
        <<<EOT
<layout-columns columns="1" cwidth="overflow"><div slot="column1"><drupal-media data-entity-type="media"></drupal-media></div>
  <div slot="column2"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],
    ];
  }

  /**
   * Tests the doublePhotoWrapper method.
   *
   * @dataProvider doublePhotoProvider
   *
   * @param $html
   *   The entered markup.
   * @param $expected
   *   The expected returned html after formatting.
   *
   * @see ::doublePhotoWrapper()
   */
  public function testDoublePhotoWrapper($html, $expected): void {
    $service = new DeprecatedWidgets($html);
    $service->doublePhotoWrapper();
    $this->assertEquals($expected, $service->getHtml());
  }

  /**
   * Provider for testDoublePhotoWrapper()
   */
  public function doublePhotoProvider() {
    // given markup, expected markup.
    return [
      [
        <<<EOT
<div class="double-photo__wrapper">
  <div class="double-photo__left"><img alt="Alt Text 1" src="http://placehold.it/1000x565">Left Caption</div>
  <div class="double-photo__right"><img alt="Alt Text 2" src="http://placehold.it/1001x566">Right Caption</div>
</div>
EOT,
        <<<EOT
<layout-columns columns="2"><div slot="column1"><img alt="Alt Text 1" src="http://placehold.it/1000x565" data-caption="Left Caption"></div>
  <div slot="column2"><img alt="Alt Text 2" src="http://placehold.it/1001x566" data-caption="Right Caption"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="double-photo__wrapper">
  <div class="double-photo__left"><img alt="Alt Text 1" src="http://placehold.it/1000x565"></div>
  <div class="double-photo__right"><img alt="Alt Text 2" src="http://placehold.it/1001x566"></div>
</div>
EOT,
        <<<EOT
<layout-columns columns="2"><div slot="column1"><img alt="Alt Text 1" src="http://placehold.it/1000x565"></div>
  <div slot="column2"><img alt="Alt Text 2" src="http://placehold.it/1001x566"></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="double-photo__wrapper">
  <div class="double-photo__left"><drupal-media data-align="center" data-entity-type="media" data-entity-uuid="123abc"></drupal-media>Left Caption</div>
  <div class="double-photo__right"><drupal-media data-align="center" data-entity-type="media" data-entity-uuid="456def"></drupal-media>Right Caption</div>
</div>
EOT,
        <<<EOT
<layout-columns columns="2"><div slot="column1"><drupal-media data-align="center" data-entity-type="media" data-entity-uuid="123abc" data-caption="Left Caption"></drupal-media></div>
  <div slot="column2"><drupal-media data-align="center" data-entity-type="media" data-entity-uuid="456def" data-caption="Right Caption"></drupal-media></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],

      [
        <<<EOT
<div class="double-photo__wrapper">
  <div class="double-photo__left"><drupal-media data-entity-uuid="123abc"></drupal-media></div>
  <div class="double-photo__right"><drupal-media data-entity-uuid="456def"></drupal-media></div>
</div>
EOT,
        <<<EOT
<layout-columns columns="2"><div slot="column1"><drupal-media data-entity-uuid="123abc"></drupal-media></div>
  <div slot="column2"><drupal-media data-entity-uuid="456def"></drupal-media></div>
  <div slot="column3"></div>
  <div slot="column4"></div>
</layout-columns>
EOT,
      ],
    ];
  }

  /**
   * Tests the personFeature method.
   *
   * @dataProvider personFeatureProvider
   *
   * @param $html
   *   The entered markup.
   * @param $expected
   *   The expected returned html after formatting.
   *
   * @see ::personFeature()
   */
  public function testPersonFeature($html, $expected): void {
    $service = new DeprecatedWidgets($html);
    $service->personFeature();
    $this->assertXmlStringEqualsXmlString($expected, $service->getHtml());
  }

  /**
   * Provider for testPersonFeature()
   */
  public function personFeatureProvider() {
    // given markup, expected markup.
    return [
      [
        <<<EOT
<aside class="clearfix person-feature width-half align-right">
  <div class="person-feature__image">Image</div>
  <h2 class="person-feature__name">Name</h2>
  <h3 class="person-feature__title">Title</h3>
  <div class="person-feature__body">
    <p>Content</p>
  </div>
  <div class="person-feature__link"><a href="#">Add a Link</a></div>
</aside>
EOT,
        <<<EOT
<feature-block class="u-width--half u-align--right" figure="true">
	<div slot="figure">Image</div>
	<div slot="title">Name</div>
	<div slot="body">
    <h4>Title</h4>
    <p>Content</p>
    <p><a class="btn--primary" href="#">Add a Link</a></p>
	</div>
</feature-block>
EOT,
      ],

      [
        <<<EOT
<aside class="person-feature width-half align-right">
  <div class="person-feature__image">Image</div>
  <h2 class="person-feature__name">Name</h2>
  <h3 class="is-hidden person-feature__title">Title</h3>
  <div class="person-feature__body">
    <p>Content</p>
  </div>
</aside>
EOT,
        <<<EOT
<feature-block class="u-width--half u-align--right" figure="true">
	<div slot="figure">Image</div>
	<div slot="title">Name</div>
	<div slot="body">
    <p>Content</p>
	</div>
</feature-block>
EOT,
      ],

      [
        <<<EOT
<aside class="person-feature">
  <div class="person-feature__image">Image</div>
  <h2 class="person-feature__name">Name</h2>
  <h3 class="is-hidden person-feature__title">Title</h3>
  <div class="person-feature__body">
    <p>Content</p>
  </div>
  <div class="person-feature__link"><a href="#">Add a Link</a></div>
</aside>
EOT,
        <<<EOT
<feature-block figure="true">
	<div slot="figure">Image</div>
	<div slot="title">Name</div>
	<div slot="body">
    <p>Content</p>
    <p><a class="btn--primary" href="#">Add a Link</a></p>
	</div>
</feature-block>
EOT,
      ],

      [
        <<<EOT
<aside class="person-feature">
  <div class="person-feature__image"></div>
  <h2 class="person-feature__name">Name</h2>
  <h3 class="is-hidden person-feature__title">Title</h3>
  <div class="person-feature__body"></div>
</aside>
EOT,
        <<<EOT
<feature-block>
	<div slot="figure"></div>
	<div slot="title">Name</div>
	<div slot="body"></div>
</feature-block>
EOT,
      ],
    ];
  }

}
