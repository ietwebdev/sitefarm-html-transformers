<?php

namespace Ucdavis\SitefarmTransformers\Tests;

use PHPUnit\Framework\TestCase;
use Ucdavis\SitefarmTransformers\Transformer\ConvertStyles;

/**
 * Class ConvertStylesTest.
 *
 * @coversDefaultClass \Ucdavis\SitefarmTransformers\Transformer\ConvertStyles
 */
class ConvertStylesTest extends TestCase {

  /**
   * Tests the process method.
   *
   * @see ::process()
   */
  public function testProcess(): void {
    $html = <<<EOT
<p style="margin-left:40px">Indent</p>
<p class="rtecenter">Text Center Aligned</p>
<div class="image"><img src="/test.png"></div>
EOT;

    $expected = <<<EOT
<p class="indent">Indent</p>
<p class="text-align-center">Text Center Aligned</p>
<div><img src="/test.png"></div>
EOT;

    $styleConverter = new ConvertStyles($html);
    $this->assertEquals($expected, $styleConverter->process());
  }

  /**
   * Tests the replaceClasses method.
   *
   * @see ::replaceClasses()
   */
  public function testReplaceClasses(): void {
    $html = <<<EOT
<section>
  <p class="rtecenter">Text Center Aligned</p>
  <p class="rteright">Text Right Aligned</p>
  <h2 class="rtecenter">Title - center</h2>
  <h2 class="rteright">Title - right</h2>
  <h2 class="category-brand__text heading--centerline">Region Title</h2>
  <p><a class="btn--pill category-brand__btn--pill" href="https://www.ucdavis.edu">Button - Pill</a></p>
  <blockquote class="pullquote--right">
    <p>Pullquote - Right</p>
  </blockquote>
  <blockquote class="pullquote--left">
    <p>Pullquote - Left</p>
  </blockquote>
  <ul class="list--divided">
    <li>Divided List</li>
  </ul>
  <ul class="list--link">
    <li><a href="#">Link List</a></li>
  </ul>
  <ul class="l-column--2 list--link">
    <li><a href="#">Link List - 2 Col</a></li>
  </ul>
  <ul class="l-column--3 list--link">
    <li><a href="#">Link List - 3 Col</a></li>
  </ul>
</section>
EOT;

    // Saving the DomDocument back to an html string causes some whitespace
    // changes so those are reflected here.
    $expected = <<<EOT
<section>
  <p class="text-align-center">Text Center Aligned</p>
  <p class="text-align-right">Text Right Aligned</p>
  <h2 class="text-align-center">Title - center</h2>
  <h2 class="text-align-right">Title - right</h2>
  <h2 class="heading--underline text-align-center">Region Title</h2>
  <p><a class="btn--primary btn--round" href="https://www.ucdavis.edu">Button - Pill</a></p>
  <blockquote class="pullquote u-align--right u-width--half">
    <p>Pullquote - Right</p>
  </blockquote>
  <blockquote class="pullquote u-align--left u-width--half">
    <p>Pullquote - Left</p>
  </blockquote>
  <ul class="list--arrow">
    <li>Divided List</li>
  </ul>
  <ul class="list--arrow">
    <li><a href="#">Link List</a></li>
  </ul>
  <ul class="l-column--2">
    <li><a href="#">Link List - 2 Col</a></li>
  </ul>
  <ul class="l-column--3">
    <li><a href="#">Link List - 3 Col</a></li>
  </ul>
</section>
EOT;

    $styleConverter = new ConvertStyles($html);
    $styleConverter->replaceClasses();
    $this->assertXmlStringEqualsXmlString($expected, $styleConverter->getHtml());
  }

  /**
   * Tests the inlineStyles method.
   *
   * @see ::inlineStyles()
   */
  public function testInlineStyles(): void {
    $html = <<<EOT
<section>
  <p style="margin-left:40px">Indent</p>
  <p style="margin-left:80px">Indent More</p>
  <p style="margin-left:120px">Indent Extreme</p>
  <p style="margin-left:40px" class="existing-class">Indent</p>
</section>
EOT;

    $expected = <<<EOT
<section>
  <p class="indent">Indent</p>
  <p class="indent--large">Indent More</p>
  <p class="indent--giant">Indent Extreme</p>
  <p class="existing-class indent">Indent</p>
</section>
EOT;

    $styleConverter = new ConvertStyles($html);
    $styleConverter->inlineStyles();
    $this->assertXmlStringEqualsXmlString($expected, $styleConverter->getHtml());
  }

  /**
   * Tests the lists method.
   *
   * @see ::removeClasses()
   */
  public function testRemoveClasses(): void {
    $html = <<<EOT
<section>
  <ul class="test-1">
    <li>Divided List</li>
  </ul>
  <ul class="test-2">
    <li><a href="#">Link List</a></li>
  </ul>
  <ul class="l-column--2 test-1">
    <li>2 Col</li>
  </ul>
  <ul class="l-column--3 test-2">
    <li>3 Col</li>
  </ul>
  <ul class="test-1 l-column--4">
    <li>3 Col</li>
  </ul>
</section>
EOT;

    $expected = <<<EOT
<section>
  <ul>
    <li>Divided List</li>
  </ul>
  <ul>
    <li><a href="#">Link List</a></li>
  </ul>
  <ul class="l-column--2">
    <li>2 Col</li>
  </ul>
  <ul class="l-column--3">
    <li>3 Col</li>
  </ul>
  <ul class="l-column--4">
    <li>3 Col</li>
  </ul>
</section>
EOT;

    $styleConverter = new ConvertStyles($html);
    $styleConverter->setRemoveClassMap([
      'test-1',
      'test-2',
    ]);
    $styleConverter->removeClasses();
    $this->assertXmlStringEqualsXmlString($expected, $styleConverter->getHtml());
  }

}
