<?php

namespace Ucdavis\SitefarmTransformers\Tests;

use PHPUnit\Framework\TestCase;
use Ucdavis\SitefarmTransformers\Transformer\IframeToEmbed;

/**
 * Class IframeToEmbedTest.
 *
 * @coversDefaultClass \Ucdavis\SitefarmTransformers\Transformer\iframeToEmbed
 */
class IframeToEmbedTest extends TestCase {

  /**
   * Tests the process method.
   *
   * @see ::process()
   */
  public function testProcess(): void {
    $html = <<<EOT
<div data-oembed-url="podbean.com/media/player/">
  <iframe src="http://soundcloud.com/tracks/930620731"></iframe>
</div>
<div data-oembed-url="https://soundcloud.com/user/track">
  <iframe src="https://soundcloud.com/user/track"></iframe>
</div>
EOT;

    $expected = <<<EOT
<drupal-url data-embed-button="url" data-embed-url="http://soundcloud.com/tracks/930620731" data-entity-label="URL" data-url-provider="SoundCloud"></drupal-url>
<drupal-url data-embed-button="url" data-embed-url="https://soundcloud.com/user/track" data-entity-label="URL" data-url-provider="SoundCloud"></drupal-url>
EOT;

    $service = new IframeToEmbed($html);
    $service->process();
    $this->assertEquals($expected, $service->getHtml());
  }

  /**
   * Tests the iframely method.
   *
   * @dataProvider iframelyProvider
   *
   * @param $html
   *   The entered markup.
   * @param $expected
   *   The expected returned html after formatting.
   *
   * @see ::iframely()
   */
  public function testIframely($html, $expected): void {
    $service = new IframeToEmbed($html);
    $service->iframely();
    $this->assertXmlStringEqualsXmlString($expected, $service->getHtml());
  }

  /**
   * Provider for testIframely().
   */
  public function iframelyProvider() {
    // given markup, expected markup.
    return [
      [
        <<<EOT
<div data-oembed-url="https://youtu.be/y17RuWkWdn8">
  <div style="height:0; left:0; padding-bottom:56.25%; position:relative; width:100%">
    <iframe
      allow="encrypted-media; accelerometer; clipboard-write; gyroscope; picture-in-picture"
      allowfullscreen=""
      scrolling="no" src="https://www.youtube.com/embed/y17RuWkWdn8?rel=0"
      style="border: 0; top: 0; left: 0; width: 100%; height: 100%; position: absolute;">
    </iframe>
  </div>
</div>
EOT,
        <<<EOT
<drupal-url data-embed-button="url" data-embed-url="https://youtu.be/y17RuWkWdn8" data-entity-label="URL" data-url-provider="YouTube"></drupal-url>
EOT,
      ],

      [
        <<<EOT
<div data-oembed-url="https://vimeo.com/415344150">
  <div style="height:0; left:0; padding-bottom:56.338%; position:relative; width:100%">
    <iframe
      allow="encrypted-media"
      allowfullscreen=""
      scrolling="no"
      src="https://player.vimeo.com/video/415344150?byline=0&amp;badge=0&amp;portrait=0&amp;title=0"
      style="border: 0; top: 0; left: 0; width: 100%; height: 100%; position: absolute;">
    </iframe>
  </div>
</div>
EOT,
        <<<EOT
<drupal-url data-embed-button="url" data-embed-url="https://vimeo.com/415344150" data-entity-label="URL" data-url-provider="Vimeo"></drupal-url>
EOT,
      ],

      [
        <<<EOT
<div data-oembed-url="https://unknown-provider.com/123">
  <iframe src="https://player.unknown-provider.com/123"></iframe>
</div>
EOT,
        <<<EOT
<drupal-url data-embed-button="url" data-embed-url="https://unknown-provider.com/123" data-entity-label="URL" data-url-provider=""></drupal-url>
EOT,
      ],
    ];
  }

  /**
   * Tests the customSoundcloud method.
   *
   * @see ::customSoundcloud()
   */
  public function testCustomSoundcloud(): void {
    $html = <<<EOT
<div data-oembed-url="//www.podbean.com/media/player/">
  <div class="responsive-iframe">&nbsp;</div>
  <iframe
    allow="autoplay"
    frameborder="no"
    height="166"
    scrolling="no"
    src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/930620731%3Fsecret_token%31234&amp;color=%2300b5e2&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true"
    width="100%">
  </iframe>
</div>
EOT;

    $expected = <<<EOT
<drupal-url data-embed-button="url" data-embed-url="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/930620731" data-entity-label="URL" data-url-provider="SoundCloud"></drupal-url>
EOT;

    $service = new IframeToEmbed($html);
    $service->customSoundcloud();
    $this->assertXmlStringEqualsXmlString($expected, $service->getHtml());
  }

}
