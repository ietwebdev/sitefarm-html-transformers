<?php

namespace Ucdavis\SitefarmTransformers\Tests;

use PHPUnit\Framework\TestCase;
use Ucdavis\SitefarmTransformers\Transformer\ConvertWidgets;

/**
 * Class ConvertWidgets.
 *
 * @coversDefaultClass \Ucdavis\SitefarmTransformers\Transformer\ConvertWidgets
 */
class ConvertWidgetsTest extends TestCase {

  /**
   * Tests the mediaLink method.
   *
   * @see ::mediaLink()
   */
  public function testMediaLink(): void {
    $html = <<<EOT
<div class="media-link__wrapper" data-url="test">
  <div class="media-link__figure">Image</div>
  <div class="media-link__body">
    <h3 class="media-link__title">Title</h3>
    <div class="media-link__content">
      <p>Content</p>
    </div>
  </div>
</div>
EOT;

    $expected = <<<EOT
<media-link url="test">
  <div slot="image">Image</div>
  <div slot="title">Title</div>
  <div slot="content"><p>Content</p></div>
</media-link>
EOT;

    $styleConverter = new ConvertWidgets($html);
    $styleConverter->mediaLink();
    $this->assertXmlStringEqualsXmlString($expected, $styleConverter->getHtml());
  }

  /**
   * Tests the featureBlock method.
   *
   * @see ::featureBlock()
   */
  public function testFeatureBlock(): void {
    $html = <<<EOT
<aside class="wysiwyg-feature-block width-half align-right">
  <h3 class="wysiwyg-feature-block__title">Title</h3>
  <div class="wysiwyg-feature-block__body">
    <p>Content</p>
  </div>
</aside>
EOT;

    $expected = <<<EOT
<feature-block class="width-half align-right">
  <div slot="figure"></div>
  <div slot="title">Title</div>
  <div slot="body"><p>Content</p></div>
</feature-block>
EOT;

    $styleConverter = new ConvertWidgets($html);
    $styleConverter->featureBlock();
    $this->assertXmlStringEqualsXmlString($expected, $styleConverter->getHtml());
  }

  /**
   * Tests the featureBlock method with nested featureBlocks.
   *
   * @see ::featureBlock()
   */
  public function testFeatureBlockNested(): void {
    $html = <<<EOT
<aside class="wysiwyg-feature-block">
  <figure class="wysiwyg-feature-block__figure">Image 1</figure>
  <h3 class="wysiwyg-feature-block__title">Title 1</h3>
  <div class="wysiwyg-feature-block__body">
    <p>Content 1</p>
    <aside class="wysiwyg-feature-block test">
      <h3 class="wysiwyg-feature-block__title">Title 2</h3>
      <div class="wysiwyg-feature-block__body"><p>Content 2</p></div>
    </aside>
  </div>
</aside>
EOT;

    $expected = <<<EOT
<feature-block figure="true">
  <div slot="figure">Image 1</div>
  <div slot="title">Title 1</div>
  <div slot="body">
    <p>Content 1</p>
    <feature-block class="test">
      <div slot="figure"></div>
      <div slot="title">Title 2</div>
      <div slot="body"><p>Content 2</p></div>
    </feature-block>
  </div>
</feature-block>
EOT;

    $styleConverter = new ConvertWidgets($html);
    $styleConverter->featureBlock();
    $this->assertXmlStringEqualsXmlString($expected, $styleConverter->getHtml());
  }

}
