<?php

require_once "../vendor/autoload.php";

use Ucdavis\SitefarmTransformers\TransformAll;

// Get the Original HTML.
$html = file_get_contents('./body.html');

// Transform all the markup.
$html = TransformAll::process($html);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Transformed WYSIWYG Text</title>
  <link rel="stylesheet" href="/css/ckeditor.css" media="all" />
  <link rel="stylesheet" href="/css/feature-block.css" media="all" />
  <link rel="stylesheet" href="/css/layout-columns.css" media="all" />
  <link rel="stylesheet" href="/css/media-link.css" media="all" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.1/css/all.css" crossorigin="anonymous">
  <link rel="stylesheet" href="http://dev.webstyleguide.ucdavis.edu/redesign/css/vendor.css" media="all" />
  <link rel="stylesheet" href="http://dev.webstyleguide.ucdavis.edu/redesign/css/style.css" media="all" />
</head>
<body>
  <h1>This is some example markup.</h1>
  <?php print $html; ?>
</body>
</html>
