# Transform Markup ready for SiteFarm

## How to use

__#1__ - Add the Bitbucket repo to `composer.json`.
```json
"repositories": {
    "ucdavis/sitefarm-transformers": {
      "type": "vcs",
      "url": "git@bitbucket.org:ietwebdev/sitefarm-html-transformers.git"
    }
}
```

__#2__ - Install the package with composer.
```
composer require ucdavis/sitefarm-transformers:dev-master
```

__#3__ - Transform all markup.

```php
use Ucdavis\SitefarmTransformers\TransformAll;

// Transform all the markup.
$transformed_html = TransformAll::process($original_html);
```

### Transform individual elements by using classes directly.

Use the `process()` method to transform all markup for a given type.
```php
use Ucdavis\SitefarmTransformers\Transformer\ConvertStyles;

$convertStyles = new ConvertStyles($original_html);
$transformed_html = $convertStyles->process();
```

Or you can directly transform a specific type.
```php
use Ucdavis\SitefarmTransformers\Transformer\ConvertStyles;

$convertStyles = new ConvertStyles($original_html);
$convertStyles->setRemoveClassMap([
  'remove-me',
  'old-class',
  'deprecated-now'
]);
$convertStyles->removeClasses();
$transformed_html = $convertStyles->getHtml();
```


## Development of this package

### Docksal

For convenience, Docksal is used to create a PHP environment for testing and
debugging.

1. Install Docksal [https://docksal.io/](https://docksal.io/)

2. Start up a local environment and see the Demo page.

```
fin start
```

#### Run unit tests.

```
fin phpunit
```
